
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var map;

var date = new Date();
var isoDate = date.toISOString().split('.')[0]+"Z";

var hospitals = [];

var stPacienta = 1;

var trenutniEhrID;


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  var ehrId = "";
  console.log(stPacienta);
  if (stPacienta == 1) {
      var ime = "Uroš";
      var priimek = "Kobal";
      var datumRojstva = "1970-01-05";
    }
    else if(stPacienta == 2){
      var ime = "Darko";
      var priimek = "Ivanov";
      var datumRojstva = "1997-02-09";
    }
    else if(stPacienta == 3){
      var ime = "Edward";
      var priimek = "Snowden";
      var datumRojstva = "1983-06-08";
    }
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              $("#preberiEHRidd").val(ehrId);
              
              sessionStorage.setItem("ehrID_trenutni", ehrId);
              trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
              document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
              console.log(trenutniEhrID);
              $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
              if(stPacienta == 1){
                zapisMeritev(ehrId, "2013-01-25T06:31Z", "175", "81", "120", "70");
                zapisMeritev(ehrId, "2014-01-25T06:31Z", "172", "78", "129", "75");
                zapisMeritev(ehrId, "2015-01-25T06:31Z", "174", "72", "130", "60");
                zapisMeritev(ehrId, "2016-01-25T06:31Z", "175", "70", "110", "80");
                zapisMeritev(ehrId, "2017-01-25T06:31Z", "170", "77", "138", "84");
                
                setTimeout(function(){
                $("#krvni-pritiski").empty();
                krvniPritisk();
                }, 70);
                  
                sessionStorage.setItem("ehrID_trenutni", ehrId);
                $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
                trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
                document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
                
                patientData();
              }else if(stPacienta == 2){
                zapisMeritev(ehrId, "2013-01-25T06:31Z", "160", "72", "122", "71");
                zapisMeritev(ehrId, "2014-01-25T06:31Z", "170", "75", "127", "67");
                zapisMeritev(ehrId, "2015-01-25T06:31Z", "179", "73", "132", "69");
                zapisMeritev(ehrId, "2016-01-25T06:31Z", "182", "74", "112", "59");
                zapisMeritev(ehrId, "2017-01-25T06:31Z", "183", "78", "134", "79");
                
                setTimeout(function(){
                $("#krvni-pritiski").empty();
                krvniPritisk();
                }, 70);
                
                sessionStorage.setItem("ehrID_trenutni", ehrId);
                $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
                trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
                document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
                
                patientData();
              }else if(stPacienta == 3){
                zapisMeritev(ehrId, "2013-01-25T06:31Z", "188", "81", "124", "72");
                zapisMeritev(ehrId, "2014-01-25T06:31Z", "186", "80", "126", "67");
                zapisMeritev(ehrId, "2015-01-25T06:31Z", "185", "88", "127", "78");
                zapisMeritev(ehrId, "2016-01-25T06:31Z", "185", "89", "134", "79");
                zapisMeritev(ehrId, "2017-01-25T06:31Z", "184", "75", "141", "81");
                
                
                setTimeout(function(){
                $("#krvni-pritiski").empty();
                krvniPritisk();
                }, 70);
                
                sessionStorage.setItem("ehrID_trenutni", ehrId);
                $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
                trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
                document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
                
                patientData();
              }
              
              if(stPacienta <= 2){
                stPacienta++;
              }else if(stPacienta >= 3){
                stPacienta = 1;
              }
              
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
    });
      }
  })
}
function zapisMeritev(ehrId, datumInUra, telesnaVisina, telesnaTeza, sistolicniKrvniTlak, diastolicniKrvniTlak) {
  
  if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
	  var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#posodobiPodatke").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
          
      },
      error: function(err) {
      	$("#posodobiPodatke").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
	
}
//document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
/*window.addEventListener("load", function(){
var mapOptions = {
  center: [46.050, 14.50],
  zoom: 13,
  maxZoom: 18
};
map = new L.map('mapid',mapOptions);
var layer = new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
map.addLayer(layer);
var popup = L.popup();

function onMapClick(e) {
  popup
  .setLatLng(e.latlng)
  .setContent("You clicked the map at " + e.latlng.toString())
  .openOn(map);
}
          
map.on('click', onMapClick);
});*/
window.addEventListener("load", function(){
  if (window.location.pathname == '/bolnisnice.html') {
    
    // Objekt oblačka markerja
    var popup = L.popup();
    
    /**
     * Dodamo izbrano oznako na zemljevid na določenih GPS koordinatah,
     * z dodtatnim opisom
     * 
     * @param lat zemljepisna širina
     * @param lng zemljepisna dolžina
     * @param o sporočilo, ki se prikaže v oblačku
     * @param type
     * @param barva (blue, red, green)
     */
    function dodajMarker(latlng, opis, tip, barva) {
      // icon settings
      var icony = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
          'marker-icon-2x-' + 
          barva + 
          '.png',
        shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
          'marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      });
        // Ustvarimo marker z vhodnima podatkoma koordinat (tocka ali poligon)
        var marker;
        if(tip == "Polygon") {
          //array kjer bomo shranili nase poligone
          for(var j = 0; j < latlng.length; j++){
            var polygon = [];
            //preslikamo cordinate
            for(var i = 0; i < latlng.length; i++){
              polygon = latlng[i].map(function(k) { return [k[1], k[0]]; });
            }
            console.log(polygon);
            if(polygon[0][0]) {
              marker = L.polygon(polygon, {color: barva});
              marker.bindPopup("<div> " + opis + "</div>");
              marker.addTo(map);
            }
          }
          
          //marker.bindPopup("<div> " + opis + "</div>").openPopup();
          //marker.addTo(map);
        }else {
          //Pointy Points
          //pointcords je 1 0 ker so podatki narobe obrneni
          var pointcords = L.latLng(latlng[1], latlng[0]);
          marker =L.marker(pointcords, {icon: icony});
          marker.bindPopup("<div> " + opis + "</div>").openPopup();
          marker.addTo(map);
        }
    }
    
    function izrisRezultatov(jsonRezultat) {
      var hospitals = jsonRezultat.features;
    
      for (var i = 0; i < hospitals.length; i++) {
        //Poberemo podatke za opis is json
        var description = hospitals[i].properties.name + "<br/>" 
        + hospitals[i].properties["addr:street"] + " " + hospitals[i].properties["addr:housenumber"];
        
    
        // pridobimo koordinate
        var coordinate = hospitals[i].geometry.coordinates;
        
        dodajMarker(coordinate, description, hospitals[i].geometry.type, "blue");
      }
    }
    
    /**
     * Dostopamo do JSON datoteke
     * in vsebino JSON datoteke vrnemo v porvratnem klicu
     */
    function getHospitaldata(callback){
      var  xobj = new XMLHttpRequest();
      xobj.overrideMimeType("application/json");
      xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
      xobj.onreadystatechange = function () {
        // rezultat ob uspešno prebrani datoteki
        if (xobj.readyState == 4 && xobj.status == "200") {
            var json = JSON.parse(xobj.responseText);
            console.log(json);
            // vrnemo rezultate
            callback(json);
        }
      };
      xobj.send(null);
    }
    
    
    
    //test 
    
    getHospitaldata(function (jsonRezultat) {
                    izrisRezultatov(jsonRezultat);
    });
   
    //test 
    
    var currentlatlng;
    var mapOptions = {
    center: [46.050, 14.50],
    zoom: 13,
    maxZoom: 18
    };
    map = new L.map('mapid',mapOptions);
    var layer = new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    map.addLayer(layer);
    
    function onMapClick(e) {
      popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(map);
    }
    
    function onLocationFound(e) {
      currentlatlng = e.latlng;
      var radius = e.accuracy / 2;
      var ikona = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
            'marker-icon-2x-' + 
            'red' + 
            '.png',
          shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
            'marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41]
      });
      var mark = L.marker(e.latlng, {icon: ikona});
      mark.bindPopup("<div> " + "STE TUKAJ!" + "</div>").openPopup();
      mark.addTo(map);
      L.marker(e.latlng).addTo(map)
        .bindPopup("<div> " + "STE TUKAJ!" + "</div>").openPopup();
      L.marker(e.latlng).addTo(map);
      L.circle(e.latlng, radius).addTo(map);
      
    }
    function onLocationError(e) {
        alert(e.message);
    }
    map.on('locationerror', onLocationError);
    map.on('locationfound', onLocationFound);    
    map.on('click', onMapClick);
    map.locate({setView: false, maxZoom: 16});
    }
  
});
  
function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva  || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              $("#preberiEHRidd").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function preberiEHRIDodBoldnika() {
  var ehrId = $("#preberiEHRid").val();
  
  if (!ehrId || ehrId.trim().length == 0) {
    $("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite EHRid stevilko!");
  }
  else {
    $.ajax({
      url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Ime: '" + party.firstNames + " " +
          party.lastNames + "', Datum rojstva: '" + party.dateOfBirth +
          "'.</span>");
          
        sessionStorage.setItem("ehrID_trenutni", ehrId);
        trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
        document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
        console.log(trenutniEhrID);
        $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
        patientData();
        krvniPritisk();
        
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
    });
  }
}

function preberiImeinPriimekBolnika() {
  var ime = $("#preberiIme").val();
  var priimek = $("#preberiPriimek").val();
  var searchData = [
    {key: "firstNames", value: $("#preberiIme").val()},
    {key: "lastNames", value: $("#preberiPriimek").val()}
    ];
  console.log(searchData);
  if (!ime || !priimek || ime.trim().length == 0 ||
      priimek.trim().length == 0 ) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite Ime in Priimek!</span>");
	} else {
	  $.ajax({
	    url: baseUrl + "/demographics/party/query",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      contentType: 'application/json',
      data: JSON.stringify(searchData),
      success: function (res) {
          for (i in res.parties) {
              var party = res.parties[i];
              var ehrId;
              for (j in party.partyAdditionalInfo) {
                  if (party.partyAdditionalInfo[j].key === 'ehrId') {
                      ehrId = party.partyAdditionalInfo[j].value;
                      break;
                      console.log(ehrId);
                  }
              }
              sessionStorage.setItem("ehrID_trenutni", party.additionalInfo["ehrId"]);
              $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
              trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
              document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
              console.log(party.firstNames);
              patientData();
              krvniPritisk();
          }
      }
	  });
	}
}

function posodobiPodatk() {
  var ehrId = $("#preberiEHRidd").val();
  var datumInUra = $("#dodajVitalnoDatumInUra").val();
  var telesnaVisina = $("#preberiTezo").val();
  var telesnaTeza = $("#").val();
  var sistolicniKrvniTlak = $("#preberiSistolicniTlak").val();
  var diastolicniKrvniTlak = $("#preberiDiastolicniTlak").val();
  
  if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
	  var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#posodobiPodatke").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
          
          krvniPritisk();
          
          sessionStorage.setItem("ehrID_trenutni", ehrId);
          $("#preberiEHRidd").val(sessionStorage.getItem("ehrID_trenutni"));
          trenutniEhrID = sessionStorage.getItem("ehrID_trenutni");
          document.getElementById("trenutniEhrIDshow").innerHTML = sessionStorage.getItem("ehrID_trenutni");
          
          patientData();
          
          var intSistolicniKrvniTlak = parseInt(sistolicniKrvniTlak);
          var intDiastolicniKrvniTlak = parseInt(diastolicniKrvniTlak);
          console.log(intSistolicniKrvniTlak);
          console.log(intDiastolicniKrvniTlak);
          if (intSistolicniKrvniTlak >= 140 || intDiastolicniKrvniTlak >= 85) {
            $('#preveripritiskOpozorilo').modal('toggle');
          }
          
      },
      error: function(err) {
      	$("#posodobiPodatke").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
	
}

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var sessionId;


function patientData() {
        return $.ajax({
            url: baseUrl + "/demographics/ehr/" + trenutniEhrID + "/party",
            type: 'GET',
            headers: {
                "Authorization": getAuthorization()
            },
            success: function (data) {
                var party = data.party;

                // Ime
                $("#patient-name").html(party.firstNames + ' ' + party.lastNames);

                // Starost
                var age = getAge(formatDateUS(party.dateOfBirth));
                $(".patient-age").html(age);

                // Datum Rojstva
                var date = new Date(party.dateOfBirth);
                var stringDate = monthNames[date.getMonth()] + '. ' + date.getDate() + ', ' + date.getFullYear();
                $(".patient-dob").html(stringDate);

                // Starost v letih
                $(".patient-age-years").html(getAgeInYears(party.dateOfBirth));
                
                //BMI
                

                /*// Spol ki ga nikjer ne generiramo
                var gender = party.gender;
                $("#patient-gender").html(gender.substring(0, 1) + gender.substring(1).toLowerCase());*/
                
                // EhrID od pacienta
                $("#patient-ehrid").html(party.additionalInfo["ehrId"])
            }
        });
}

  
  
function krvniPritisk() {
        //alert("caller is " + krvniPritisk.caller);
        if( $('#krvni-pritiski').is(':empty') ) {
        }else{
         $("#krvni-pritiski").empty(); 
        }
        var colors = ['#8A0707', '#AD5151'];
        setTimeout(function(){
          return $.ajax({
            url: baseUrl + "/view/" + trenutniEhrID + "/blood_pressure",
            type: 'GET',
            headers: {
                "Authorization": getAuthorization()
            },
            success: function (res) {
                res.forEach(function (el, i, arr) {
                    var date = new Date(el.time);
                    el.date = date.getTime();
                }); 
               // $("#krvni-pritiski").empty();
                new Morris.Area({
                    element: 'krvni-pritiski',
                    data: res.reverse(),
                    xkey: 'date',
                    ykeys: ['systolic', 'diastolic'],
                    lineColors: colors,
                    labels: ['Systolic', 'Diastolic'],
                    lineWidth: 2,
                    pointSize: 3,
                    hideHover: true,
                    behaveLikeLine: true,
                    smooth: false,
                    resize: true,
                    xLabels: "day",
                    xLabelFormat: function (x) {

                        var date = new Date(x);

                        return (date.getDate() + '-' + monthNames[date.getMonth()]);
                    },
                    dateFormat: function (x) {

                        return (formatDate(x, false));
                    }
                });
                console.log(res);
                //last measurement
                var bp = res[res.length - 1].systolic + "/" + res[res.length - 1].diastolic + " " + res[res.length - 1].unit;
                $('.last-bp').text(bp);
                $('.last-bp-date').text(formatDate(res[res.length - 1].time, true));
            }
        });
        }, 50);
        /*return $.ajax({
            url: baseUrl + "/view/" + trenutniEhrID + "/blood_pressure",
            type: 'GET',
            headers: {
                "Authorization": getAuthorization()
            },
            success: function (res) {
                res.forEach(function (el, i, arr) {
                    var date = new Date(el.time);
                    el.date = date.getTime();
                }); 
               // $("#krvni-pritiski").empty();
                new Morris.Area({
                    element: 'krvni-pritiski',
                    data: res.reverse(),
                    xkey: 'date',
                    ykeys: ['systolic', 'diastolic'],
                    lineColors: colors,
                    labels: ['Systolic', 'Diastolic'],
                    lineWidth: 2,
                    pointSize: 3,
                    hideHover: true,
                    behaveLikeLine: true,
                    smooth: false,
                    resize: true,
                    xLabels: "day",
                    xLabelFormat: function (x) {

                        var date = new Date(x);

                        return (date.getDate() + '-' + monthNames[date.getMonth()]);
                    },
                    dateFormat: function (x) {

                        return (formatDate(x, false));
                    }
                });
                console.log(res);
                //last measurement
                var bp = res[res.length - 1].systolic + "/" + res[res.length - 1].diastolic + " " + res[res.length - 1].unit;
                $('.last-bp').text(bp);
                $('.last-bp-date').text(formatDate(res[res.length - 1].time, true));
            }
        }); */
}
  
  
  
  
  
  
  
  // Pomozne funkcije
   function getAge(dateString) {
        var now = new Date();
        var today = new Date(now.getYear(), now.getMonth(), now.getDate());

        var yearNow = now.getYear();
        var monthNow = now.getMonth();
        var dateNow = now.getDate();

        var dob = new Date(dateString.substring(6, 10),
                dateString.substring(0, 2) - 1,
            dateString.substring(3, 5)
        );

        var yearDob = dob.getYear();
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        var age = {};
        var ageString = "";
        var yearString = "";
        var monthString = "";
        var dayString = "";


        var yearAge = yearNow - yearDob;

        if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
        else {
            yearAge--;
            var monthAge = 12 + monthNow - monthDob;
        }

        if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
        else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }

        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
        };

        if (age.years > 1) yearString = "y";
        else yearString = "y";
        if (age.months > 1) monthString = "m";
        else monthString = "m";
        if (age.days > 1) dayString = " days";
        else dayString = " day";


        if ((age.years > 0) && (age.months > 0) && (age.days > 0))
            ageString = age.years + yearString + " " + age.months + monthString;// + ", and " + age.days + dayString + " old";
        else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
            ageString = age.days + dayString + " old";
        else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
            ageString = age.years + yearString;// + " old. Happy Birthday!";
        else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
            ageString = age.years + yearString + " and " + age.months + monthString;// + " old";
        else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
            ageString = age.months + monthString; // + " and " + age.days + dayString + " old";
        else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
            ageString = age.years + yearString;// + " and " + age.days + dayString + " old";
        else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
            ageString = age.months + monthString;// + " old";
        else ageString = "Oops! Could not calculate age!";

        return ageString;
    }

    function formatDate(date, completeDate) {

        var d = new Date(date);

        var curr_date = d.getDate();
        curr_date = normalizeDate(curr_date);

        var curr_month = d.getMonth();
        curr_month++;
        curr_month = normalizeDate(curr_month);

        var curr_year = d.getFullYear();

        var curr_hour = d.getHours();
        curr_hour = normalizeDate(curr_hour);

        var curr_min = d.getMinutes();
        curr_min = normalizeDate(curr_min);

        var curr_sec = d.getSeconds();
        curr_sec = normalizeDate(curr_sec);

        var dateString, monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        if (completeDate){
            dateString = curr_date + "-" + monthNames[curr_month-1] + "-" + curr_year + " at " + curr_hour + ":" + curr_min; // + ":" + curr_sec;
        }
        else dateString = curr_date + "-" + monthNames[curr_month-1] + "-" + curr_year;

        return dateString;

    }

    function formatDateUS(date) {
        var d = new Date(date);

        var curr_date = d.getDate();
        curr_date = normalizeDate(curr_date);

        var curr_month = d.getMonth();
        curr_month++;
        curr_month = normalizeDate(curr_month);

        var curr_year = d.getFullYear();

        return curr_month + "-" + curr_date + "-" + curr_year;

    }

    function getAgeInYears(dateOfBirth) {
        var dob = new Date(dateOfBirth);
        var timeDiff = Math.abs(Date.now() - dob.getTime());
        return Math.floor(timeDiff / (1000 * 3600 * 24 * 365));
    }

    function normalizeDate(el) {
        el = el + "";
        if (el.length == 1) {
            el = "0" + el;
        }
        return el;
    }